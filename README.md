#Solução proposta por Andre Postiga

O index.html se encontra em SITE -> Base -> index.html

Estrutura de pastas

    SITE
    ├───Base
    │  ├──assets
    │  │    ├──src
    │  │        ├──arquivos
    │  │        │   ├──img
    │  │        │   ├──contato.css
    │  │        │   ├──contato.scss
    │  │        │   ├──main-contato.js
    │  │        │
    │  │        ├──scss
    │  │            ├──base
    │  │                ├──base.scss
    │  │                ├──buttons.scss
    │  │                ├──config.scss
    │  │                ├──fonts.scss
    │  │            ├──components
    │  │                ├──banner.scss
    │  │                ├──components.scss
    │  │                ├──footer.scss
    │  │                ├──header.scss
    │  │            ├──pages
    │  │                ├──common.scss
    │  │                ├──contato.scss
    │  │                ├──pages.scss
    │  │
    │  ├──templates
    │  │    ├──subtemplates
    │  │        ├──banner.html
    │  │        ├──contato.html
    │  │        ├──footer.html
    │  │        ├──header.html
    │  │        
    │  ├──index.html
    │
    │
    │
    │
    ├──Helpers
        ├──mixins.scss
        ├──reset.scss  




#Ideia: Utilizar frameworks bourbon-neat para resolver o problema proposto.

A estruturação de pastas foi escolhida baseada em projetos passados, a escolha
pela separação de componentes, configurações/variáveis e páginas, facilida na hora
de separar o CSS e deixar o código mais pragmático.

O layout desenvolvido visou utilizar a menor quantidade possível de CSS, utilizando técnicas
de flexbox, reaproveitando código com mixins, grids do neat e mixins de compatibilidade. Como
o conteúdo javascript utilizado foi muito pequeno optei por fazer a seleção da DOM com JavaScript 
puro.

Infelizmente não consegui fazer o layout responsivo, seja por falta de experiência ou de costume com o 
framework Neat, e, a restrição de não poder utilizar bootstrap. 


#--------------------------------------------------------------------

# Seja um desenvolvedor na E-Can

Você pode clonar esse repositório e fazer seu teste em uma nova branch ou 
enviar o código por E-mail.

#### Regras:
*  Desenvover o layout na pasta PSD 
*  A página preicsa ser responsiva 
* Não utilizar bootstrap 

#### Bonus:
* Pré processado de CSS (SASS) 
* Automátizador de tarefas (Grunt, gulp) 
* Web Server 
* JavaScript (Jquery)

#### Requisitos desejáveis:
* Html5, css3;
* Javascript, jquery;
* Versionamento Git;
* Grunt;
* Sass;
* Sites responsivos;
* Cross-browser (IE10+);
* Iniciativa, criatividade e colaboração;
* Boas práticas: reutilização de código, semântica, organização, performance.

#### Objetivo do teste:
* Avaliar capacidade componentização e otimização de código
* Avaliar reutilização de código 
* Avaliar desenvolvimento responsivo